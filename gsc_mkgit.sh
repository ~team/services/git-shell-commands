#!/bin/sh
# Git shell command for make a Git repository
# Copyright (C) 2021 Márcio Silva <coadde@hyperbola.info>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Check if 'dirname' is installled.
if [ $(command -v dirname > /dev/null) ]; then
	printf 'Error!: dirname executable not found.\n'
	exit 1
# Check if Git is installled.
elif [ $(command -v git > /dev/null) ]; then
	printf 'Error!: Git executable not found.\n'
	exit 1
# Check if 'mkdir' is installled.
elif [ $(command -v mkdir > /dev/null) ]; then
	printf 'Error!: mkdir executable not found.\n'
	exit 1
fi

source $(dirname $0)/lib/gsclib.sh

function gslib_mkdgrpf
{
	# Execute 'gslib_checkf' shell function and
	# import GSCMD_PATH variable.
	gslib_checkf

	# Import error status number from 'gslib_checkf'.
	local LOCAL_ERROR=$?
	case $LOCAL_ERROR	in
		0)		cd $GSCMD_PATH		;;
		*)		return $LOCAL_ERROR	;;
	esac
	unset LOCAL_ERROR

	printf 'Please specify the address of the Git repository:\n'
	local LOCAL_PATH
	read LOCAL_PATH

	# Import error status number from 'read' builtin command.
	local LOCAL_EREAD=$?
	case $LOCAL_EREAD	in
		0)					;;
		*)		return 10		;;
	esac
	unset LOCAL_EREAD

	if [ -z $LOCAL_PATH ]; then
		LOCAL_PATH=.
	elif [ $(echo $LOCAL_PATH | \
	    sed 's|.*[.]git|.git|; s|[.]git[/].*|.git|') = .git ]; then
		printf 'Error!: %s directory contains .git suffix.\n' \
		    $LOCAL_PATH
		return 254
	fi

	# Check if address of the Git repository is available.
	if [ -e $LOCAL_PATH ]; then
		printf 'Warning!: %s directory was created.\n' $LOCAL_PATH

		case $USER	in
			git)	if [ -z $LOCAL_PATH ]; then
					printf \
					    'Error!: This path is the main directory.\n' \
					return 255
				fi
				;;
			*)	;;
		esac

		if [ ! -d $LOCAL_PATH ]; then
			printf 'Error!: %s path is not directory.\n' \
			    $LOCAL_PATH
			return 12
		elif [ ! -g $LOCAL_PATH ]; then
			printf \
			    'Error!: %s directory requires SGID permission.\n' \
			    $LOCAL_PATH
			return 13
		elif [ -u $LOCAL_PATH ]; then
			printf \
			    'Error!: %s directory contains SUID permission.\n' \
			    $LOCAL_PATH
			return 14
		elif [ -k $LOCAL_PATH ]; then
			printf \
			    'Error!: %s directory contains sticky bit permission.\n' \
			    $LOCAL_PATH
			return 15
		elif [ ! -r $LOCAL_PATH ]; then
			printf 'Error!: %s directory is not readable.\n' \
			    $LOCAL_PATH
			return 16
		elif [ ! -w $LOCAL_PATH ]; then
			printf 'Error!: %s directory is not writable.\n' \
			    $LOCAL_PATH
			return 17
		elif [ ! -x $LOCAL_PATH ]; then
			printf 'Error!: %s directory is not accessable.\n' \
			    $LOCAL_PATH
			return 18
		# Check if address of the Git repository contains
		# two periods (../) directory.
		elif [ $(echo $LOCAL_PATH | \
		    sed 's|.*[/][.][.]|..|; s|[.][.][/].*|..|') = .. ]; then
			printf \
			    'Error!: %s path contains two periods directory.\n' \
			    $LOCAL_PATH
			return 19
		fi
	elif [ ! -e $LOCAL_PATH ]; then
		case $USER	in
			git)	# Check if address of the Git repository contains
				# subdirectories or / character.
				if [ ! $(echo $LOCAL_PATH | \
				    sed 's|.*[/]|/|; s|[/].*|/|') = / ]; then
					printf 'Error!: %s main directory not found.\n' \
					    $LOCAL_PATH
					return 11
				fi
				;;
			*)	;;
		esac

		mkdir $LOCAL_PATH
	fi

	cd $LOCAL_PATH
	unset LOCAL_PATH

	return 0
}

function gslib_mkgitf
{
	# Execute 'gslib_mkdgrpf' shell function.
	gslib_mkdgrpf

	# Import error status number from 'gslib_mkdgrpf'.
	local LOCAL_ERROR=$?
	case $LOCAL_ERROR	in
		0)					;;
		*)		return $LOCAL_ERROR	;;
	esac
	unset LOCAL_ERROR

	printf 'Please specify the name of the Git repository:\n'
	local LOCAL_NAME
	read LOCAL_NAME

	# Import error status number from 'read' builtin command.
	local LOCAL_EREAD=$?
	case $LOCAL_EREAD	in
		0)					;;
		*)		return 20		;;
	esac
	unset LOCAL_EREAD

	if [ -z $LOCAL_NAME ]; then
		printf 'Error!: %s the name of repository is null.\n' \
		    $LOCAL_NAME
		return 29
	fi

	# Check if Git repository is available.
	if [ -e $LOCAL_NAME.git ]; then
		printf 'Warning!: %s.git directory was created.\n' $LOCAL_NAME

		if [ ! -d $LOCAL_NAME.git ]; then
			printf \
			    'Error!: %s.git repository is not directory.\n' \
			    $LOCAL_NAME
			return 22
		elif [ ! -g $LOCAL_NAME.git ]; then
			printf \
			    'Error!: %s.git directory requires SGID permission.\n' \
			    $LOCAL_NAME
			return 23
		elif [ -u $LOCAL_NAME.git ]; then
			printf \
			    'Error!: %s.git directory contains SUID permission.\n' \
			    $LOCAL_NAME
			return 24
		elif [ -k $LOCAL_NAME.git ]; then
			printf \
			    'Error!: %s.git directory contains sticky bit permission.\n' \
			    $LOCAL_NAME
			return 25
		elif [ ! -r $LOCAL_NAME.git ]; then
			printf 'Error!: %s.git directory is not readable.\n' \
			    $LOCAL_NAME
			return 26
		elif [ ! -w $LOCAL_NAME.git ]; then
			printf 'Error!: %s.git directory is not writable.\n' \
			    $LOCAL_NAME
			return 27
		elif [ ! -x $LOCAL_NAME.git ]; then
			printf \
			    'Error!: %s.git directory is not accessable.\n' \
			    $LOCAL_NAME
			return 28
		elif [ -e $LOCAL_NAME.git/HEAD ]; then
			printf \
			    'Error!: %s.git directory is not empty.\n' \
			    $LOCAL_NAME
			return 21
		fi
	elif [ ! -e $LOCAL_NAME.git ]; then
		mkdir $LOCAL_NAME.git
	fi

	# create Git repository.
	git init --bare $LOCAL_NAME.git

	# Import error status number from Git command.
	local LOCAL_EGIT=$?
	case $LOCAL_EGIT	in
		0)					;;
		*)		return 30		;;
	esac
	unset LOCAL_EGIT

	unset LOCAL_NAME

	return 0
}

# Execute 'gslib_mkgitf' shell function.
gslib_mkgitf

# Exit with error status number from 'gslib_mkgitf'.
exit $?
