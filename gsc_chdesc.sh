#!/bin/sh
# Git shell command for change description in a Git repository
# Copyright (C) 2021 Márcio Silva <coadde@hyperbola.info>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Check if 'dirname' is installled.
if [ $(command -v dirname > /dev/null) ]; then
	printf 'Error!: dirname executable not found.\n'
	exit 1
# Check if 'find' is installled.
elif [ $(command -v find > /dev/null) ]; then
	printf 'Error!: find executable not found.\n'
	exit 1
# Check if 'nano' is installled.
elif [ $(command -v nano > /dev/null) ]; then
	printf 'Error!: nano executable not found.\n'
# Check if 'cat' is installled.
elif [ $(command -v cat > /dev/null) ]; then
	printf 'Error!: cat executable not found.\n'
	exit 1
fi

source $(dirname $0)/lib/gsclib.sh

function gslib_chdescf
{
	# Execute 'gslib_checkf' shell function and
	# import GSCMD_PATH variable.
	gslib_checkf

	# Import error status number from 'gslib_checkf'.
	local LOCAL_ERROR=$?
	case $LOCAL_ERROR	in
		0)		cd $GSCMD_PATH		;;
		*)		return $LOCAL_ERROR	;;
	esac
	unset LOCAL_ERROR

	# Print a list of Git repositories without './' and '/HEAD'.
	local LOCAL_CHDESCLIST
	LOCAL_CHDESCLIST=($(find . -name HEAD | sed 's|^[.][/]||; s|[/]HEAD||'))

	# Import error status number from 'find' command.
	local LOCAL_EFIND=$?
	case $LOCAL_EFIND	in
		0)					;;
		*)		return 10		;;
	esac
	unset LOCAL_EFIND

	local LOCAL_LIST
	local LOCAL_NUMBER=0
	for LOCAL_LIST in ${LOCAL_CHDESCLIST[@]}; do
		printf "%i %s\n" $LOCAL_NUMBER $LOCAL_LIST
		LOCAL_NUMBER=$(($LOCAL_NUMBER + 1))
	done
	unset LOCAL_LIST
	unset LOCAL_NUMBER

        printf 'Please specify the Git repository number to change description:\n'
        local LOCAL_NUMBER
        read LOCAL_NUMBER

	# Import error status number from 'read' builtin command.
	local LOCAL_EREAD=$?
	case $LOCAL_EREAD	in
		0)					;;
		*)		return 11		;;
	esac
	unset LOCAL_EREAD

	printf 'The %s Git repository description are:\n' ${LOCAL_CHDESCLIST[$LOCAL_NUMBER]}
	cat ${LOCAL_CHDESCLIST[$LOCAL_NUMBER]}/description

	# Import error status number from 'cat' command.
	local LOCAL_ECAT=$?
	case $LOCAL_ECAT	in
		0)					;;
		*)		return 12		;;
	esac
	unset LOCAL_ECAT

	printf 'Are you sure you want to change description?\n'
        local LOCAL_CONFIRM
        read LOCAL_CONFIRM

	# Import error status number from 'read' builtin command.
	local LOCAL_EREAD=$?
	case $LOCAL_EREAD	in
		0)					;;
		*)		return 11		;;
	esac
	unset LOCAL_EREAD

	case $LOCAL_CONFIRM	in
		1)											;&
		[yY])											;&
		[yY][eE])										;&
		[yY][eE][sS])										;&
		[tT])											;&
		[tT][rR])										;&
		[tT][rR][uU])										;&
		[tT][rR][uU][eE])	nano -w ${LOCAL_CHDESCLIST[$LOCAL_NUMBER]}/description		;;
		*)			printf 'The change description procedure was canceled\n'	;;
	esac

	return 0
}

# Execute 'gslib_chdescf' shell function.
gslib_chdescf

# Exit with error status number from 'gslib_chdescf'.
exit $?
